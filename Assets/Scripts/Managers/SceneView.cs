﻿using UnityEngine;
using System.Collections;

public class SceneView : MonoBehaviour
{
		/*
	 * Not currently used. Intended as a generic scene builder for larger scenes with many assets
	 * 
	 * */

		public Transform[] objects;
		public Vector3[] positions;
		public Vector3[] rotations;


		// Use this for initialization
		void Start ()
		{
				for (int i = 0; i<objects.Length; i++) {
						objects [i].position = positions [i];
						objects [i].rotation = Quaternion.Euler (rotations [i]);
				}

		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
