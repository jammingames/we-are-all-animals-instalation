﻿using UnityEngine;
using System.Collections;
using System;

public class SceneManager : Singleton<SceneManager>
{

		
		public GameObject[] scenes;	 						//an array of all scenes to instantiate/destroy on timing					
		GameObject currentScene;							//reference to current scene
		public DateTime currentDate = DateTime.Now;			//public for accesibility from GUI
		DateTime startDate;									//reference to original start date for calculations

		public delegate void SpawnObject (int hour); 		//delegate/event to spawn random objects at random times depending on time of day/scene
		public event SpawnObject OnSpawn;

		public void TriggerSpawn (int hour)					//call this to fire the event
		{
				if (OnSpawn != null) {
						OnSpawn (hour);
				}
		}




		void OnEnable ()									//hook in and remove event methods in OnEnable and OnDisable to prevent weak links
		{
				GUIManager.instance.OnGuiSliderEvent += ChangeTime;
				this.OnSpawn += CheckTimeAndChangeScene;
				this.AfterFadeEvent += InitAfterFade;
				this.StartFadeEvent += PauseBeforeFade;
		}

		void OnDisable ()
		{
				if (GUIManager.instance != null)
						GUIManager.instance.OnGuiSliderEvent -= ChangeTime;

				this.OnSpawn -= CheckTimeAndChangeScene;
				this.AfterFadeEvent -= InitAfterFade;
				this.StartFadeEvent -= PauseBeforeFade;
		}
		
		
		IEnumerator Start ()
		{
				//endDate is created 18 hours in the future from startDate (length of program running)
				startDate = currentDate;
				var hour = currentDate.AddHours (18).Hour;
				DateTime endDate = new DateTime (currentDate.Year, currentDate.Month, currentDate.Day, hour, currentDate.Minute, currentDate.Second);

				//just to ensure manager is initialized from InitManager
				yield return Auto.Wait (1);
				currentScene = GameObject.Instantiate (scenes [0], Vector3.zero, Quaternion.identity) as GameObject;
				
				
				
		}

		
		

		
		float timer = 1;
		float minTimer = 60;
		public bool doTick = false;
		void Update ()
		{
				if (!doTick)
						return;
				//Timer used to add seconds to Realtime, affected by Time.timeScale
				if (timer > 0)
						timer -= Time.deltaTime;
				else if (timer <= 0) {

						currentDate = currentDate.AddSeconds (1);
						timer = 1;
				}
				//Timer for checking current scene and attempting to spawn an object once per minute
				if (minTimer > 0)
						minTimer -= Time.deltaTime;
				else if (minTimer <= 0) {
						TriggerSpawn (currentDate.Hour);
						minTimer = 60;
				}
		}

		//changes time by minutes and calls checkTime 
		void ChangeTime (float from, float to, float val)
		{
				DateTime newDate = startDate;
				currentDate = newDate.AddMinutes (val);
				CheckTimeAndChangeScene (currentDate.Hour);
		}


		//logic to load up various scenes and destroy old ones based on current hour
		// note, this can easily be extended into a generic system for scenes/subscenes, with generic actions on particular hours.
		void CheckTimeAndChangeScene (int hour)
		{
				if (hour == 13 && !currentScene.name.Contains ("2")) {
						currentScene = DestroyAndSpawn (scenes [1]);
				} else if (hour == 14 && !currentScene.name.Contains ("3")) {
						currentScene = DestroyAndSpawn (scenes [2]);
				} else if (hour == 15 && !currentScene.name.Contains ("4")) {
						currentScene = DestroyAndSpawn (scenes [3]);
				} else if (hour < 13 || hour > 15) {
						if (!currentScene.name.Contains ("1")) {
								currentScene = DestroyAndSpawn (scenes [0]);
						}
				}
				
		}


		//quick helper class
		GameObject DestroyAndSpawn (GameObject gObj)
		{	
				TriggerStartFade (1);
				GameObject.Destroy (currentScene);
				return GameObject.Instantiate (gObj, Vector3.zero, Quaternion.identity) as GameObject;
		}

		void InitAfterFade (int fadeTimes)
		{
				//if (fadeTimes < 1)
				doTick = true;
		}

		void PauseBeforeFade (int fadeTimes)
		{
				//if (fadeTimes > 0)
				doTick = false;
		}



		public delegate void AfterFade (int hour); 		
		public event AfterFade AfterFadeEvent, StartFadeEvent;

	
		public void TriggerAfterFade (int hour)
		{
				if (AfterFadeEvent != null) {
						AfterFadeEvent (hour);
				}
		}
	
		public void TriggerStartFade (int hour)
		{
				if (StartFadeEvent != null) {
						StartFadeEvent (hour);
				}
		}


}
