﻿using UnityEngine;
using System.Collections;

public class GUIManager : Singleton<GUIManager>
{



		public GUISkin gSkin;
		public GUIStyle gStyle;
		public float minVal = 0;
		public float maxVal = 1080; // 18hrs *60min = 1080 min
		


		void Start ()
		{
		}

		float hSliderValue = 0.0F;
		void OnGUI ()
		{
				if (!SceneManager.instance.doTick)
						return;
				GUI.skin = gSkin;
				float tempVal = hSliderValue;

				//horizontal slider bar for purpose of scrubbing through timeline
				hSliderValue = GUI.HorizontalSlider (new Rect ((Screen.width / 2) - 175, Screen.height - Screen.height / 6.5f, 350, 120), hSliderValue, minVal, maxVal);

				// if there is a change in the slider Value, send an event with the info
				if (hSliderValue != tempVal) {
						TriggerGuiEvent (minVal, maxVal, hSliderValue);
				}
		
		}


		/// <summary>
		/// GuiEvents to fire information to other scripts when needed
		/// </summary>
		public delegate void GuiSliderEvent (float from,float to,float val);
		public event GuiSliderEvent OnGuiSliderEvent;
		public delegate void GuiEvent (float val);
		public event GuiEvent OnGuiEvent;
	
		public void TriggerGuiEvent (float from, float to, float val)
		{
				if (OnGuiSliderEvent != null) {
						OnGuiSliderEvent (from, to, val);
				}
		}
	
		public void TriggerGuiEvent (float val)
		{
				if (OnGuiEvent != null) {
						OnGuiEvent (val);
				}
		}
}
