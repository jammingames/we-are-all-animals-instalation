﻿using UnityEngine;
using System.Collections;

public class SphereMovement : MonoBehaviour
{

		public Vector3 targetPosition1, targetPosition2;
		Vector3 origPos;
		public Vector2 range1, range2, range3;
		public EaseType easer;


		void OnEnable ()
		{
				SceneManager.instance.OnSpawn += SpawnSphere;
		}

		void OnDisable ()
		{
				if (SceneManager.instance != null)
						SceneManager.instance.OnSpawn -= SpawnSphere;
		}


		// Use this for initialization
		IEnumerator Start ()
		{
				origPos = transform.position;

				//waits between 1-15 seconds before first animation plays bnccccccccccccccccccccccccccccccccccccccccccccccccbv                            	         AAA
				yield return Auto.Wait (Random.Range (1, 15));
				MoveSphere ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.T))
						MoveSphere ();
		}



		//called by the trigger spawn event. uses current hour + random 0-24 to determine if it should play the sphere movement
		void SpawnSphere (int hour)
		{

				if (Random.Range (0, 24) % hour == 0)
						MoveSphere (hour);
		}

		
	


	
		void MoveSphere (int hour)
		{
				//3 nested tweens, each with a separate randomized time range, additionally current hour adds 0-18 possibly randomization
				//this can be tweaked for absolute full control of alll animation timings using the System.Action callback
				StartCoroutine (transform.MoveTo (targetPosition1, Random.Range (range1.x, range1.y) + Random.Range (0, hour), easer, () => {
						StartCoroutine (transform.MoveTo (new Vector3 (targetPosition2.x + Random.Range (-hour / 2, hour / 2), targetPosition2.y + Random.Range (-hour / 2, hour / 2), targetPosition2.z + Random.Range (-hour / 2, hour / 2)), Random.Range (range2.x, range2.y) + Random.Range (0, hour), easer, () => {
								StartCoroutine (transform.MoveTo (origPos, Random.Range (range3.x, range3.y) + Random.Range (0, hour), easer, null));
						}));
				}));
		}


		void MoveSphere ()
		{

				StartCoroutine (transform.MoveTo (targetPosition1, Random.Range (range1.x, range1.y), easer, () => {
						StartCoroutine (transform.MoveTo (targetPosition2, Random.Range (range2.x, range2.y), easer, () => {
								StartCoroutine (transform.MoveTo (origPos, Random.Range (range3.x, range3.y), easer, null));
						}));
				}));
		}
}
