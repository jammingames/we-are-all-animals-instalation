﻿using UnityEngine;
using System.Collections;

public class TimeScaleText : MonoBehaviour
{

		GUIText _guiText;


		void Start ()
		{
				_guiText = GetComponent<GUIText> ();
		}
	
		
		void Update ()
		{
				_guiText.text = "Playback speed: " + Time.timeScale + "x.  A slow, D fast, S reset";
		}
}
